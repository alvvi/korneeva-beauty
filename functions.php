<?php
/**
 * Korneeva Beauty Landing Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package kb
 */

 /**
 * Define constants for paths, globals, etc. 
 */
define( 'THEMEROOT', get_template_directory_uri() );
define( 'THEME_DIST', THEMEROOT . '/dist' );
define( 'THEME_ASSETS', THEMEROOT . '/dist/assets' );

if ( ! function_exists( 'kb_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kb_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on Korneeva Beauty Landing Theme, use a find and replace
		* to change 'kb' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'kb', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'kb_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 30,
		'width'       => 185,
		'flex-width'  => true,
		'flex-height' => true,
	) );

	/**
	 * Register custom image sizes
	 */
	// add_image_size( 'special-card-img', 280, 170, true );
	
}
endif;
add_action( 'after_setup_theme', 'kb_setup' );



/**
 * Require Theme Dependecies
 * 
 */

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Helper functions
 */
require get_template_directory() . '/inc/utils.php';


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'title' => 'Общие поля',
		'slug' => 'common_fields',
	));
	
}


if ( ! function_exists( 'kb_scripts' ) ) :
/**
 * Enqueue scripts and styles.
 */
function kb_scripts() {
	wp_enqueue_style( 'kb-style', THEME_DIST . '/css/index.css' );
	wp_register_script( 'kb-commons', THEME_DIST . '/js/commons.js', array(), null, true );
	wp_register_script( 'kb-scripts', THEME_DIST . '/js/index.js', array(), null, true );
	wp_enqueue_style( 'kb-wp-style', THEMEROOT . '/style.css' );

	wp_enqueue_script('kb-commons');
	wp_enqueue_script('kb-scripts');
}
endif; 
add_action( 'wp_enqueue_scripts', 'kb_scripts' );

/**
 * Remove auto wrapping in P tags from Contact Form 7 plugin if present 
 */
add_filter('wpcf7_autop_or_not', '__return_false');