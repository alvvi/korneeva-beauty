<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kb
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEME_ASSETS ?>/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo THEME_ASSETS ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo THEME_ASSETS ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo THEME_ASSETS ?>/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo THEME_ASSETS ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="<?php echo THEME_ASSETS ?>/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#00a300">
	<meta name="msapplication-config" content="<?php echo THEME_ASSETS ?>/favicon/browserconfig.xml">
	<meta name="theme-color" content="#96398D">

	<meta name="yandex-verification" content="810c4157f2da8228" /> 
</head>

<body <?php body_class(); ?>>

<?php get_template_part('template-parts/sprite'); ?>
<div class="bg-layer"></div>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'kb' ); ?></a>

	<?php
		$logo = get_field('logo', 'option');
		$header_info = get_field('header_info', 'option');

	if ( is_front_page() ) :
	?>
	<header id="masthead" class="header">
		<div class="header__grid grid">
			<div class="header__row row row--align_center row--justify_center">
				<div class="header__col col col--12">
					<a href="#" class="db">
						<?php 
							echo wp_get_attachment_image($logo['id'], 'full', false, array('class' => 'responsive-img'));
						?>
					</a>
					<p><?php echo $header_info; ?></p>
				</div>
			</div>
		</div>
	</header>
	<?php
	endif;
	?>

	<div id="content" class="site-content">