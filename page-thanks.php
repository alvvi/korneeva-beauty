<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kb
 */

get_header();
?>

<section class="section thanks">
    <div class="thanks__grid grid">
        <div class="thanks__row row">
            <div class="thanks__col col col--12 col--lg_6">
                <img src="<?php echo THEME_ASSETS ?>/img/person2.jpg" alt="" class="thanks__img responsive-img">
            </div>
            <div class="thanks__content col col--12 col--sm_8 col--lg_4">
                <h2 class="thanks__heading heading heading--md">
                    Проверьте
                    <br><span class="color-accent">вашу почту</span>
                </h2>

                <p>Подарок с подтверждением вашей<br> почты уже на вашем ящике</p>
                <p class="highlight"><span>Обязательно подтвердите ваш e-mail,<br> иначе вы не получите ccылку на<br> вебинар!!!</span></p>
                <p class="info">Если письма во «Входящих» нет, проверьте папку «Спам» <br> или закладку «Промоакции» (если у вас почта Gmail). <br> Всякое бывает ;)</p>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();