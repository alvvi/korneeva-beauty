<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package kb
 */

get_header();

$prices_heading = get_field('prices_heading');
$prices_list = get_field('prices_list');
?>

<section class="section prices"> 
    <div class="prices__heading-grid grid">
        <h2 class="prices__heading heading heading--md">
            <?php echo $prices_heading; ?>
        </h2>
    </div>
    <div class="prices__content grid">
        <img src="<?php echo THEME_ASSETS ?>/img/prices-decor.svg" alt="" class="prices__decor">
        <div class="prices__row row row--gutters_none">
            <?php
            foreach ( $prices_list as $index => $prices_item ) :
                $is_center = $index === 1;
                $name = $prices_item['name'];
                $old_price = $prices_item['old_price'];
                $price = $prices_item['price'];
                $list = $prices_item['list'];
                $features = $prices_item['features'];
                ?>
                <div class="prices__col col col--12 col--lg_4">
                    <article class="prices__card prices-card">
                        <header class="prices-card__header">
                            <h3 class="prices-card__heading"><?php echo $name; ?></h3>
                            <div class="prices-card__old-price"><?php echo number_format( $old_price, 0, '.', ' ' ); ?> руб.</div>
                            <div class="prices-card__price"><?php echo number_format( $price, 0, '.', ' ' ); ?> руб.</div>
                            <?php
                            if ( $is_center ) :
                                ?>
                                <div class="prices-card__tag tag">популярный пакет</div>
                                <?php
                            endif;
                            ?>
                        </header>

                        <div class="prices-card__body">
                            <ul class="prices-card__list">
                                <?php
                                foreach ( $list as $item ) :
                                    ?>
                                    <li class="prices-card__item">
                                        <?php 
                                            echo $item['text']; 
                                            if ( $item['bonus'] ) :
                                                ?>
                                                <span class="prices-card__tag tag tag--small">bonus</span>
                                                <?php 
                                            endif;
                                        ?>
                                    </li>
                                    <?php
                                endforeach;
                                ?>
                            </ul>
                        </div>

                        <footer class="prices-card__footer">
                            <div class="prices-card__features">
                                <h3 class="prices-card__heading">зачет</h3>
                                <ul class="prices-card__list">
                                    <?php
                                    foreach ( $features as $item ) :
                                        ?>
                                        <li class="prices-card__item">
                                            <?php
                                            echo $item['text']; 
                                            if ( $item['bonus'] ) :
                                                ?>
                                                <span class="prices-card__tag tag tag--small">bonus</span>
                                                <?php 
                                            endif;
                                            ?>
                                        </li>
                                        <?php
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                            <button data-micromodal-trigger="<?php echo $prices_item['modal_id']; ?>" class="prices-card__button button">заказать</button>
                        </footer>
                    </article>
                </div>
                <?php
            endforeach;
            ?>

        </div>
    </div>
</section>

<?php
get_template_part('template-parts/modals');
get_footer();