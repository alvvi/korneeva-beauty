<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kb
 */

?>

	</div><!-- #content -->

	<?php
	$requisites = get_field('requisites', 'option');
	$footer_links = get_field('footer_links', 'option');
	?>
	<footer id="footer" class="footer">

		<?php
		if ( is_page('thanks') ) :
			?>
			<div class="footer__grid grid">
				<div class="footer__form">
					<div class="footer__form-wrapper">
						<script id="749ca1a38abc9471e86dbf3a2f3847c837803843" src="http://korneevabeauty.getcourse.ru/pl/lite/widget/script?id=44967"></script>
					</div>
				</div>
			</div>
			<?php
		endif;
		?>

		<div class="footer__grid grid">
			<div class="footer__row row">
				<div class="footer__col footer__col--info col col--12 col--lg_4">
					<?php echo $requisites; ?>
				</div>
				<div class="footer__col footer__col--links col col--12 col--lg_4">
					<?php
					foreach ( $footer_links as $link ) :
						?>
						<a class="footer__link link" href="<?php echo $link['url']['url']; ?>" target="_blank"><?php echo $link['text']; ?></a>
						<?php
					endforeach;
					?>
				</div>
				<div class="footer__col footer__col--logo col col--12 col--lg_4">
					<a href="http://360-media.ru/" target="_blank">
						<img src="<?php echo THEME_ASSETS ?>/img/360.png" alt="360 Media">
					</a>
				</div>
			</div>
		</div>
	</footer>
</div><!-- #page -->

<style>
	html {
		margin-top: 0 !important;
	}
</style>

<?php wp_footer(); ?>

</body>
</html>
