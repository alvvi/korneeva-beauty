<?php
/**
 * The template for displaying the landing page
 *
 * @package kb
 */

get_header();
?>

<?php 

$hero_headline = get_field('hero_headline');
$hero_heading = get_field('hero_heading');
$hero_subheading = get_field('hero_subheading');
$hero_content = get_field('hero_content');
$hero_form_text = get_field('hero_form_text');

?>
<section class="section hero">
    <div class="hero__grid grid">
        <div class="hero__row row">
            <div class="hero__content col col--12">

                <h1 class="hero__heading"><?php echo $hero_heading; ?></h1>

                <p><?php echo $hero_content; ?></p>
                
                <button class="button hero__button" data-scroll-to="#register">Зарегистрироваться</button>
                
                <?php /* echo do_shortcode('[contact-form-7 id="5" title="Контактная форма 1" html_class="hero__form form"]'); */ ?>

                <div class="hero__info"><?php echo $hero_form_text; ?></div>
                <svg class="hero__arrow" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="64" version="1.1" height="64" viewBox="0 0 64 64" enable-background="new 0 0 64 64">
                    <g>
                        <path fill="#1D1D1B" d="m63.386,16.193l.002-.002c0.002-0.003 0.004-0.006 0.006-0.01 0.172-0.195 0.298-0.43 0.399-0.678 0.032-0.076 0.053-0.148 0.076-0.225 0.058-0.191 0.094-0.389 0.106-0.596 0.006-0.076 0.018-0.148 0.016-0.226 0-0.04 0.01-0.076 0.008-0.115-0.014-0.239-0.062-0.47-0.136-0.687-0.006-0.023-0.022-0.041-0.03-0.064-0.088-0.239-0.214-0.451-0.363-0.645-0.021-0.027-0.028-0.063-0.05-0.09l-10.311-12.146c-0.789-0.93-2.084-0.948-2.894-0.037-0.808,0.91-0.823,2.402-0.032,3.334l5.558,6.549c-8.121-1.076-16.104,0.633-16.481,0.717-24.646,4.467-42.087,27.227-38.88,50.736 0.159,1.164 1.028,1.992 2.019,1.992 0.106,0 0.212-0.009 0.32-0.027 1.116-0.203 1.878-1.409 1.704-2.696-2.857-20.94 13.056-41.282 35.537-45.358 0.103-0.024 8.351-1.794 16.117-0.574l-8.577,5.093c-1.005,0.598-1.398,2.02-0.881,3.177 0.516,1.159 1.748,1.608 2.756,1.017l13.52-8.028c0.183-0.111 0.347-0.25 0.491-0.411z" style="fill: #96398D;"></path>
                    </g>
                </svg>
            </div>
        </div>
    </div>
    <button data-micromodal-trigger="modal-video" class="hero__play-btn">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 496.158 496.158" style="enable-background:new 0 0 496.158 496.158;" xml:space="preserve">
            <path style="fill:#96398D;" d="M496.158,248.085c0-137.021-111.07-248.082-248.076-248.082C111.07,0.002,0,111.062,0,248.085  c0,137.002,111.07,248.071,248.083,248.071C385.088,496.155,496.158,385.086,496.158,248.085z"/>
            <path style="fill:#FFFFFF;transform: scale(0.8) translate(10%, 10%);" d="M370.805,235.242L195.856,127.818c-4.776-2.934-11.061-3.061-15.951-0.322  c-4.979,2.785-8.071,8.059-8.071,13.762v214c0,5.693,3.083,10.963,8.046,13.752c2.353,1.32,5.024,2.02,7.725,2.02  c2.897,0,5.734-0.797,8.205-2.303l174.947-106.576c4.657-2.836,7.556-7.986,7.565-13.44  C378.332,243.258,375.452,238.096,370.805,235.242z"/>
        </svg>
    </button>
</section>

<?php
$find_out_heading = get_field('find_out_heading');
$find_out_steps = get_field('find_out_steps');

if ($find_out_steps) :
    ?>
    <section class="section find-out">
        <div class="find-out__grid grid">

            <div class="find-out__heading-row row">
                <div class="find-out__col col col--12">
                    <h2 class="find-out__heading heading"><?php echo $find_out_heading; ?></h2>
                </div>
            </div>

            <div class="find-out__content-row row">
                <?php
                foreach ( $find_out_steps as $index => $step ) :
                    ?>
                    <div class="find-out__col col col--12 col--lg_6">
                        <div class="find-out__item">
                            <div class="find-out__number"><?php echo $index + 1; ?></div>
                            <p class="find-out__text"><?php echo $step['content']; ?></p>
                        </div>
                    </div>
                    <?php
                endforeach;
                ?>

            </div>

        </div>
    </section>
    <?php
endif;
?>

<?php

$person_heading = get_field('person_heading');
$person_img = get_field('person_img');
$person_content = get_field('person_content');
?>
<section class="section person">
    <div class="person__grid grid">
        <div class="person__row row">
            <div class="person__heading-col col col--12 col--lg_6">
                <h2 class="person__heading heading"><?php echo $person_heading; ?></h2> 
                <?php echo wp_get_attachment_image( $person_img['id'], 'full', false, array(
                    'class' => 'responsive-img'
                ) ); ?>
            </div>
            <div class="person__content col col--12 col--lg_6">
                <?php echo $person_content; ?>
            </div>
        </div>
    </div>
</section>
<?php
$visit_heading = get_field('visit_heading');
$visit_grid = get_field('visit_grid');

if ($visit_grid) : 
    ?>
    <section class="section visit">
        <div class="visit__grid grid">
            <div class="visit__heading-row row">
                <div class="visit__col col col--12">
                    <h2 class="visit__heading heading heading"><?php echo $visit_heading; ?></h2>
                </div>
            </div>
            <div class="visit__benifits b-benefit-grid">
            <?php
            foreach ( $visit_grid as $cell ) :
                $sprite_id = $cell['sprite_id'];
                $heading = $cell['heading'];
                $text = $cell['text'];
                $img = $cell['img'];
                ?>
                <div class="b-benefit-grid__item">
                    <div class="b-benefit-grid__item-content">
                        <div class="b-benefit-grid__item-content-inner">
                            <svg class="b-benefit-grid__item-icon" width="27" height="39">
                                <use xlink:href="#<?php echo $sprite_id; ?>"></use>
                            </svg>
                            <h3 class="b-benefit-grid__item-caption"><?php echo $heading; ?></h3>
                            <div><?php echo $text; ?></div>
                        </div>
                    </div>
                    <?php echo wp_get_attachment_image($img['id'], 'full', false, array('class' => 'b-benefit-grid__item-cover')) ?>
                </div>
                <?php 
            endforeach;
            ?>

            </div>
        </div>
    </section>
    <?php
endif;
?>

<?php

$register_heading = get_field('register_heading');
$regsiter_text = get_field('regsiter_text');
?>

<section id="register" class="section register">
    <div class="register__grid grid">
        <div class="register__row row">
            <div class="register__col col col--12">
                <h2 class="register__heading heading"><?php echo $register_heading; ?></h2>
                <p class="register__text"><?php echo $regsiter_text; ?></p>
                <?php // echo do_shortcode('[contact-form-7 id="5" title="Контактная форма 1" html_class="register__form form"]'); ?>
            </div>

            <div class="register__col col col--12">
                <script id="1efcbb99b11d1fbffbbd168fbb3ff5c8b0ba71a5" src="http://korneevabeauty.getcourse.ru/pl/lite/widget/script?id=44757"></script>
            </div>
        </div>
    </div>
</section>

<?php
get_template_part('template-parts/modals');
get_footer();
