import MicroModal from '@/js/vendor/micromodal.min.js'
import $ from 'jquery'

MicroModal.init({
    disableScroll: true,
    disableFocus: true,
    onClose: (modal) => {
        document.querySelector('body').style.overflow = 'auto'

        if ( modal.id === 'modal-video' ) {
            changeVideoState('pauseVideo')
        }
    }
})

$('[data-micromodal-trigger="modal-video"]').on('click', (e) => {
    // const $target = $(e.currentTarget)
    // const videoUrl = $target.data('video-url')
    // const $modal = $('#modal-video')

    // const iframeVideo = `
    //     <iframe class="yvideo" width="560" height="315" src="${videoUrl}?enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    // `

    // $modal.find('.modal__dynamic-content').empty().append(iframeVideo)
})


function changeVideoState(state) {
    var div = document.getElementById('modal-video')
    var iframe = div.getElementsByTagName('iframe')[0].contentWindow
    iframe.postMessage('{"event":"command","func":"' + state + '","args":""}', '*')
}