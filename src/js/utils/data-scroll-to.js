import jump from 'jump.js'

const elements = document.querySelectorAll('[data-scroll-to]')

function handleClick(e) {
    const clickTarget = e.currentTarget
    const scrollTarget = clickTarget.getAttribute('data-scroll-to')
    
    jump(scrollTarget)
}

Array.from(elements).forEach(el => {
    el.addEventListener('click', handleClick);
})