<div class="modal modal--form" id="modal-prices-basic" aria-hidden="true">
	<div class=modal__overlay tabindex=-1 data-micromodal-close>
		<div class="modal__grid grid">
			<div class="modal__container" role="dialog" aria-modal="true" data-micromodal-close>
				<div class="modal__form-wrapper" data-micromodal-close>
					<button class="modal__close close" data-micromodal-close=data-micromodal-close>
						<svg class=modal__svg viewBox="0 0 32 32" data-micromodal-close=data-micromodal-close>
							<use class=modal__icon xlink:href=#close data-micromodal-close=data-micromodal-close></use>
						</svg>
					</button>
					<script id="44cfe786b58c36eed80f0dfe4b708eed535b563d" src="http://korneevabeauty.getcourse.ru/pl/lite/widget/script?id=44802"></script>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal modal--form" id="modal-prices-standart" aria-hidden="true">
	<div class=modal__overlay tabindex=-1 data-micromodal-close>
		<div class="modal__grid grid">
			<div class="modal__container" role="dialog" aria-modal="true" data-micromodal-close>
				<div class="modal__form-wrapper" data-micromodal-close>
					<button class="modal__close close" data-micromodal-close=data-micromodal-close>
						<svg class=modal__svg viewBox="0 0 32 32" data-micromodal-close=data-micromodal-close>
							<use class=modal__icon xlink:href=#close data-micromodal-close=data-micromodal-close></use>
						</svg>
					</button>
					<script id="c53c2dfef78c74f284723ff7abf4296e462da782" src="http://korneevabeauty.getcourse.ru/pl/lite/widget/script?id=44822"></script>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal modal--form" id="modal-prices-expanded" aria-hidden="true">
	<div class=modal__overlay tabindex=-1 data-micromodal-close>
		<div class="modal__grid grid">
			<div class="modal__container" role="dialog" aria-modal="true" data-micromodal-close>
				<div class="modal__form-wrapper" data-micromodal-close>
					<button class="modal__close close" data-micromodal-close=data-micromodal-close>
						<svg class=modal__svg viewBox="0 0 32 32" data-micromodal-close=data-micromodal-close>
							<use class=modal__icon xlink:href=#close data-micromodal-close=data-micromodal-close></use>
						</svg>
					</button>
					<script id="e00e5dfb64e196f4e05deacf1f5430e010bee417" src="http://korneeva-beauty.ru/pl/lite/widget/script?id=44824"></script>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_template_part('template-parts/modal-video'); ?>