<div class="modal modal--media" id=modal-video aria-hidden=true>
    <div class=modal__overlay tabindex=-1 data-micromodal-close=data-micromodal-close>
        <div class="modal__grid grid">
            <div class=modal__container role=dialog aria-modal=true aria-labelledby=modal-video-title aria-describedby=modal-video-content>
                <div class=modal__document role=document>
                    <div class="modal__row row row--centred">
                        <div class="modal__col col col--12" data-micromodal-close=data-micromodal-close>
                            <div class="modal__preloader preloader"><img class=preloader__icon src="<?php echo THEME_ASSETS ?>/preloader.gif" alt="" role=presentation /> </div>
                            <div class="modal__close close" data-micromodal-close=data-micromodal-close>
                                <svg class=modal__svg viewBox="0 0 32 32" data-micromodal-close=data-micromodal-close>
                                    <use class=modal__icon xlink:href=#close data-micromodal-close=data-micromodal-close>
                                    </use>
                                </svg>
                            </div>
                            <div class="modal__dynamic-content">
                                <div class="modal__responsive responsive-video">
                                    <iframe class=responsive-video__video width=560 height=315 src="https://www.youtube.com/embed/_zAbztj83po?enablejsapi=1" frameborder=0 allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>